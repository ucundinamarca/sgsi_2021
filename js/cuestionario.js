const contenedor1 = document.getElementById('test1');
const botonRespuesta1 = document.getElementById('boton1');
const resultadoTest1 = document.getElementById('resultado1');

const preguntas1 = [
    {
        pregunta1: "¿El Sistema de Gestión de Seguridad de la Información a que Macroproceso del Sistema Integrado de Gestión pertenece?",
        respuestas1: {
            a: "Estratégico",
            b: "Misional",
            c: "Apoyo",
            d: "Seguimiento, Medición, Análisis y evaluación"
        },
        respuestaCorrecta1: "a"
    },
    {
        pregunta1: "¿El Modelo de Gestión Integral de Seguridad de la Información, opera a través de cuantas Etapas?",
        respuestas1: {
            a: "8 Etapas",
            b: "6 Etapas",
            c: "3 Etapas",
            d: "Ninguna de las anteriores"
        },
        respuestaCorrecta1: "b"
    },
    {
        pregunta1: "¿Cuáles son las etapas del Modelo de Gestión Integral de Seguridad de la Información?",
        respuestas1: {
            a: "Gestión de Activos de la Información y Gestión de Riesgos",
            b: "Gestión de Incidentes y Gestión de Cumplimiento",
            c: "Gestión de la Continuidad del Negocio y Gestión del Cambio y Cultura",
            d: "Todas las anteriores"
        },
        respuestaCorrecta1: "d"
    },
    {
        pregunta1: "¿Cuál es la resolución por la cual se establece una política de seguridad de la información en la Universidad de Cundinamarca?",
        respuestas1: {
            a: "Resolución 000050 de 2018",
            b: "Resolución 050 de 2018",
            c: "Resolución 088 de 2017",
            d: "Resolución 000088 de 2017"
        },
        respuestaCorrecta1: "c"
    },
    {
        pregunta1: "¿Qué significa la sigla SGSI?",
        respuestas1: {
            a: "Soporte de Generación de Sistemas de Información",
            b: "Sistema de Gestión de Seguridad de la Información",
            c: "Soporte de Gestión del Sistema Informático",
            d: "Sistema de Generación de Seguridad Informática"
        },
        respuestaCorrecta1: "b"
    }
];

function mostrarTest1 () {
    const preguntasYrespuestas1 = [];

    preguntas1.forEach ((preguntaActual1, numeroDePregunta1) => {
        const respuestas1 = [];

        for (letraRespuesta1 in preguntaActual1.respuestas1) {

            respuestas1.push(
                `<label style="margin-top: 5px; display: flex;">
                    <input type="radio" name="${numeroDePregunta1}" value="${letraRespuesta1}">
                    <div id="${numeroDePregunta1}${letraRespuesta1}">${preguntaActual1.respuestas1[letraRespuesta1]}</div>
               </label>`
            );
        };

        preguntasYrespuestas1.push(
            `<div style="width:470px;">
            <div class="cuestion1" style="margin: 10px;"> <b>${preguntaActual1.pregunta1}</b> </div>
            <div class="respuestas1" style="margin: 10px; padding: 0px 15px; display: flex; flex-direction: column; "> ${respuestas1.join('')} </div>
            </div>
            `
        );
    });

    contenedor1.innerHTML = preguntasYrespuestas1.join('');
};

mostrarTest1();

let conteo1 = 0;

function mostrarResultado1 () {
    const respuestas1 = contenedor1.querySelectorAll('.respuestas1');
    let respuestasCorrectas1 = 0;

        preguntas1.forEach ((preguntaActual1, numeroDePregunta1) => {
            const todasLasRespuestas1 = respuestas1[numeroDePregunta1];
            const checkBoxRespuestas1 = `input[name='${numeroDePregunta1}']:checked`;
            const respuestaElegida1 = (todasLasRespuestas1.querySelector(checkBoxRespuestas1) || {}).value;
            // let incorrectas = document.querySelectorAll('.respuestas1');
            // incorrectas.classList.remove('incorrecta');

            if (respuestaElegida1 === preguntaActual1.respuestaCorrecta1) {
                document.getElementById(numeroDePregunta1+respuestaElegida1).classList.add('correcta');
                respuestasCorrectas1++;
            } else {
                document.getElementById(numeroDePregunta1+respuestaElegida1).classList.add('incorrecta');
            };
        });

        if (conteo1 == 1) {
            document.getElementById('boton1').setAttribute('style', 'display: none;');
            document.getElementById('btn_siguiente_14').classList.remove('invisible');
        }
        conteo1++;

    resultadoTest1.innerHTML = 'Usted ha acertado <strong>' + respuestasCorrectas1 + '</strong> de 5 respuestas.';
};

botonRespuesta1.addEventListener('click', mostrarResultado1);

// **************************
// *** Cuestionario modulo 2.1
// **************************

const contenedor21 = document.getElementById('test21');
const botonRespuesta21 = document.getElementById('boton21');
const resultadoTest21 = document.getElementById('resultado21');

const preguntas21 = [
    {
        pregunta21: "¿Cuál es el código del MANUAL DE ADMINISTRACIÓN DE RIESGOS DE SEGURIDAD Y PRIVACIDAD DE LA INFORMACIÓN?",
        respuestas21: {
            a: "ESG-SSI-P11",
            b: "ESG-SSI-P04",
            c: "ESG-SSI-M003",
            d: "ESG-SSI-M009"
        },
        respuestaCorrecta21: "d"
    },
    {
        pregunta21: "Cualquier información o elemento relacionado con el tratamiento de datos, que tenga valor para la organización, se denomina:",
        respuestas21: {
            a: "Información",
            b: "Confidencialidad",
            c: "Activo de la información",
            d: "Dato personal"
        },
        respuestaCorrecta21: "c"
    },
    {
        pregunta21: "La propiedad que determina la información como accesible y utilizable por solicitud de una entidad autorizada, cuando ésta así lo requiera, se denomina:",
        respuestas21: {
            a: "Disponibilidad",
            b: "Activo de la Información",
            c: "Privacidad",
            d: "Integridad"
        },
        respuestaCorrecta21: "a"
    },
    {
        pregunta21: "Parte designada por la institución, director, jefe o gestor responsable del proceso, que tiene el compromiso de garantizar que la información y los activos se clasifiquen adecuadamente.",
        respuestas21: {
            a: "Funcionario",
            b: "Propietario",
            c: "Usuario",
            d: "Estudiante "
        },
        respuestaCorrecta21: "b"
    },
    {
        pregunta21: '¿Cuál es la codificación para el activo de la información tipo “Hardware”?',
        respuestas21: {
            a: "HD",
            b: "HW",
            c: "HR",
            d: "HE"
        },
        respuestaCorrecta21: "b"
    }
];

function mostrarTest21 () {
    const preguntasYrespuestas21 = [];

    preguntas21.forEach ((preguntaActual21, numeroDePregunta21) => {
        const respuestas21 = [];

        for (letraRespuesta21 in preguntaActual21.respuestas21) {

            respuestas21.push(
                `<label style="margin-top: 5px; display: flex;">
                    <input type="radio" name="${numeroDePregunta21}" value="${letraRespuesta21}">
                    <div id="21${numeroDePregunta21}${letraRespuesta21}">${preguntaActual21.respuestas21[letraRespuesta21]}</div>
               </label>`
            );
        };

        preguntasYrespuestas21.push(
            `<div style="width:470px;">
            <div class="cuestion21" style="margin: 10px;"> <b>${preguntaActual21.pregunta21}</b> </div>
            <div class="respuestas21" style="margin: 10px; padding: 0px 15px; display: flex; flex-direction: column; "> ${respuestas21.join('')} </div>
            </div>
            `
        );
    });

    contenedor21.innerHTML = preguntasYrespuestas21.join('');
};

mostrarTest21();

let conteo21 = 0;

function mostrarResultado21 () {
    const respuestas21 = contenedor21.querySelectorAll('.respuestas21');
    let respuestasCorrectas21 = 0;

        preguntas21.forEach ((preguntaActual21, numeroDePregunta21) => {
            const todasLasRespuestas21 = respuestas21[numeroDePregunta21];
            const checkBoxRespuestas21 = `input[name='${numeroDePregunta21}']:checked`;
            const respuestaElegida21 = (todasLasRespuestas21.querySelector(checkBoxRespuestas21) || {}).value;

            if (respuestaElegida21 === preguntaActual21.respuestaCorrecta21) {
                document.getElementById('21'+numeroDePregunta21+respuestaElegida21).classList.add('correcta');
                respuestasCorrectas21++;
            } else {
                document.getElementById('21'+numeroDePregunta21+respuestaElegida21).classList.add('incorrecta');
            };
        });

        if (conteo21 == 1) {
            document.getElementById('boton21').setAttribute('style', 'display: none;');
            document.getElementById('btn_siguiente_38').classList.remove('invisible');
        }
        conteo21++;

    resultadoTest21.innerHTML = 'Usted ha acertado <strong>' + respuestasCorrectas21 + '</strong> de 5 respuestas.';
};

botonRespuesta21.addEventListener('click', mostrarResultado21);

// **************************
// *** Cuestionario modulo 2.2
// **************************

const contenedor22 = document.getElementById('test22');
const botonRespuesta22 = document.getElementById('boton22');
const resultadoTest22 = document.getElementById('resultado22');

const preguntas22 = [
    {
        pregunta22: "¿Qué se realiza en el procedimiento ESG-SSI-P03?",
        respuestas22: {
            a: "Recolección de datos personales",
            b: "Almacenamiento de datos personales",
            c: "Modificación de datos personales",
            d: "Eliminación de datos personales"
        },
        respuestaCorrecta22: "a"
    },
    {
        pregunta22: "¿Qué tipos de datos hay?",
        respuestas22: {
            a: "Personal, público y sensible",
            b: "Público, semipúblico y sensible",
            c: "Público, privado y delicado",
            d: "Público y sensible"
        },
        respuestaCorrecta22: "a"
    },
    {
        pregunta22: "¿Quién es el responsable del tratamiento de los datos en la Institución?",
        respuestas22: {
            a: "Dirección de Talento Humano",
            b: "Universidad de Cundinamarca",
            c: "Superintendencia de Industria y Comercio",
            d: "Dirección de Control Interno"
        },
        respuestaCorrecta22: "b"
    },
    {
        pregunta22: "Los datos que afectan la intimidad del titular o cuyo uso indebido puede generar su discriminación, se denominan:",
        respuestas22: {
            a: "Datos sensibles",
            b: "Datos administrativos",
            c: "Datos semiprivados",
            d: "Datos financieros"
        },
        respuestaCorrecta22: "a"
    },
    {
        pregunta22: "¿Cuál de los siguientes no es un principio dispuesto en la Política de Tratamiento de Datos Personales de la Universidad de Cundinamarca?",
        respuestas22: {
            a: "Principio de finalidad",
            b: "Principio de claridad",
            c: "Principio de transparencia",
            d: "Principio de seguridad"
        },
        respuestaCorrecta22: "b"
    }
];

function mostrarTest22 () {
    const preguntasYrespuestas22 = [];

    preguntas22.forEach ((preguntaActual22, numeroDePregunta22) => {
        const respuestas22 = [];

        for (letraRespuesta22 in preguntaActual22.respuestas22) {

            respuestas22.push(
                `<label style="margin-top: 5px; display: flex;">
                    <input type="radio" name="${numeroDePregunta22}" value="${letraRespuesta22}">
                    <div id="22${numeroDePregunta22}${letraRespuesta22}">${preguntaActual22.respuestas22[letraRespuesta22]}</div>
               </label>`
            );
        };

        preguntasYrespuestas22.push(
            `<div style="width:470px;">
            <div class="cuestion22" style="margin: 10px;"> <b>${preguntaActual22.pregunta22}</b> </div>
            <div class="respuestas22" style="margin: 10px; padding: 0px 15px; display: flex; flex-direction: column; "> ${respuestas22.join('')} </div>
            </div>
            `
        );
    });

    contenedor22.innerHTML = preguntasYrespuestas22.join('');
};

mostrarTest22();

let conteo22 = 0;

function mostrarResultado22 () {
    const respuestas22 = contenedor22.querySelectorAll('.respuestas22');
    let respuestasCorrectas22 = 0;

        preguntas22.forEach ((preguntaActual22, numeroDePregunta22) => {
            const todasLasRespuestas22 = respuestas22[numeroDePregunta22];
            const checkBoxRespuestas22 = `input[name='${numeroDePregunta22}']:checked`;
            const respuestaElegida22 = (todasLasRespuestas22.querySelector(checkBoxRespuestas22) || {}).value;

            if (respuestaElegida22 === preguntaActual22.respuestaCorrecta22) {
                document.getElementById('22'+numeroDePregunta22+respuestaElegida22).setAttribute('style', 'font-weight: bold; color: rgb(11, 218, 28);');
                respuestasCorrectas22++;
            } else {
                document.getElementById('22'+numeroDePregunta22+respuestaElegida22).setAttribute('style', 'font-weight: bold; color: red;');
            };
        });

        if (conteo22 == 1) {
            document.getElementById('boton22').setAttribute('style', 'display: none;');
            document.getElementById('btn_siguiente_62').classList.remove('invisible');
        }
        conteo22++;

    resultadoTest22.innerHTML = 'Usted ha acertado <strong>' + respuestasCorrectas22 + '</strong> de 5 respuestas.';
};

botonRespuesta22.addEventListener('click', mostrarResultado22);
